<?php
/*
Plugin Name: Mon premier plugin
Plugin URI: https://mon-siteweb.com/
Description: Ceci est mon premier plugin
Author: Mon nom et prénom ou celui de ma société
Version: 1.0
Author URI: http://mon-siteweb.com/
*/

add_action( 'woocommerce_review_order_before_submit',  'wcpw_check_order' ); 
function wcpw_check_order($order_id){
    //$order = new WC_Order( $order_id );
    $order = new WC_Cart( $order_id );
    var_dump($order);
}


/* after an order has been processed, we will use the  'woocommerce_thankyou' hook, to add our function, to send the data */
add_action('woocommerce_payment_complete', 'wdm_send_order_to_ext'); 
function wdm_send_order_to_ext( $order_id ){
    $order = new WC_Order( $order_id );
    if($order->billing_email != ""){
        createOrder($order_id );
    } 

 }

function createOrder($order_id){
    // get order object and order details
    $order = new WC_Order( $order_id ); 
    $email = $order->billing_email;
    $phone = $order->billing_phone;
    $shipping_type = $order->get_shipping_method();
    $shipping_cost = $order->get_total_shipping();
    $postcode = $order->get_billing_postcode();


    // set the address fields
    $user_id = $order->user_id;
    $address_fields = array('country',
        'title',
        'first_name',
        'last_name',
        'company',
        'address_1',
        'address_2',
        'address_3',
        'address_4',
        'city',
        'state',
        'postcode');

    $address = array();
    if(is_array($address_fields)){
        foreach($address_fields as $field){
            $address['billing_'.$field] = get_user_meta( $user_id, 'billing_'.$field, true );
            $address['shipping_'.$field] = get_user_meta( $user_id, 'shipping_'.$field, true );
        }
    }
    
    // get coupon information (if applicable)
    $cps = array();
    $cps = $order->get_items( 'coupon' );
    
    $coupon = array();
    foreach($cps as $cp){
            // get coupon titles (and additional details if accepted by the API)
            $coupon[] = $cp['name'];
    }
    
    // get product details
    $items = $order->get_items();
    
    $item_name = array();
    $item_qty = array();
    $item_price = array();
    $item_sku = array();
        
    foreach( $items as $key => $item){
        $item_name[] = $item['name'];
        $item_qty[] = $item['qty'];
        $item_price[] = $item['line_total'];
        
        $item_id = $item['product_id'];
        $product = new WC_Product($item_id);
        $item_sku[] = $product->get_sku();
    }
    
    /* for online payments, send across the transaction ID/key. If the payment is handled offline, you could send across the order key instead */
    $transaction_key = get_post_meta( $order_id, '_transaction_id', true );
    $transaction_key = empty($transaction_key) ? $_GET['key'] : $transaction_key;   
    
    // set the username and password
    $api_username = '66149cf7-c47d-49d0-9277-36779a76271e';
    $api_password = 'test_a68e3f75-588f-4430-8dab-b9444aaa96ef';

    // to test out the API, set $api_mode as ‘sandbox’
    $api_mode = 'sandbox';
    if($api_mode == 'sandbox'){
        // sandbox URL example
        $endpoint = "https://sandbox.pwinty.com/v3.0"; 
    }
    else{
        // production URL example
        $endpoint = "http://example.com/"; 
    }
        // setup the data which has to be sent
    $data = array(
            'apiuser'                   => $api_username,
            'apipass'                   => $api_password,
            'customer_email'            => $email,
            'customer_phone'            => $phone,
            'countryCode'               => 'GB',
            'recipientName'             => $address['billing_first_name'].' '.$address['billing_last_name'],
            'address1'                  => $address['billing_address_1'],
            'addressTownOrCity'         => $address['billing_city'],
            'stateOrCountry'            => $address['billing_state'],
            'postalOrZipCode'           => $address['billing_postcode'],
            'transaction_key'           => $transaction_key,
            'preferredShippingMethod'   => "Express"
            );

            // send API request via cURL
        $ch = curl_init();

        /* set the complete URL, to process the order on the external system. Let’s consider http://example.com/buyitem.php is the URL, which invokes the API */
        curl_setopt($ch, CURLOPT_URL, $endpoint."/orders");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Pwinty-MerchantId: 66149cf7-c47d-49d0-9277-36779a76271e",
            "X-Pwinty-REST-API-Key: test_a68e3f75-588f-4430-8dab-b9444aaa96ef",
            "Content-Type: application/x-www-form-urlencoded"
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        $response = curl_exec ($ch);
        $response_data = json_decode($response);
        $id = null;
        if ($response_data) {
            $id = $response_data->data->id;
        }
    
        curl_close ($ch);
        
        // the handle response    
        if (strpos($response,'ERROR') !== false) {
            //print('error');    
            //print_r($response);
            echo "error";
        } else {

            if ($id) {
                addImageToOrder($id, $order_id);
            }
        }
}

 function addImageToOrder($id, $order_id){

    $order = new WC_Order( $order_id ); 

    // set the username and password
    $api_username = '66149cf7-c47d-49d0-9277-36779a76271e';
    $api_password = 'test_a68e3f75-588f-4430-8dab-b9444aaa96ef';

    $data = array(
        'sku'                       => 'GLOBAL-HPR-16X24',
        'url'                       => 'http://awayoftravel.fr/wp-content/uploads/2020/03/Randonne-etang-soulcem-pyrenees-ariege-blog-184-1-67x67.jpg',
        'copies'                    => '1',
        'sizing'                    => 'Crop',
        'priceToUser'               => '59',
        );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://sandbox.pwinty.com/v3.0/orders/'.$id.'/images');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    
    $headers = array();
    $headers[] = 'X-Pwinty-Merchantid: 66149cf7-c47d-49d0-9277-36779a76271e';
    $headers[] = 'X-Pwinty-Rest-Api-Key: test_a68e3f75-588f-4430-8dab-b9444aaa96ef';
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    print_r($result);
 }

 function getSubmissionStatus(){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://sandbox.pwinty.com/v3.0/orders/851304/SubmissionStatus');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    
    
    $headers = array();
    $headers[] = 'X-Pwinty-Merchantid: 66149cf7-c47d-49d0-9277-36779a76271e';
    $headers[] = 'X-Pwinty-Rest-Api-Key: test_a68e3f75-588f-4430-8dab-b9444aaa96ef';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
 }
 
function submitOrder(){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://sandbox.pwinty.com/v3.0/orders/851304/status');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "status=Submitted");

    $headers = array();
    $headers[] = 'X-Pwinty-Merchantid: 66149cf7-c47d-49d0-9277-36779a76271e';
    $headers[] = 'X-Pwinty-Rest-Api-Key: test_a68e3f75-588f-4430-8dab-b9444aaa96ef';
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
}
