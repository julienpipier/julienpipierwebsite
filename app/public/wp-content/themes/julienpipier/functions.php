<?php 
/**
 * jp functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jp
 */

if ( ! function_exists( 'jp_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jp_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on jp, use a find and replace
		 * to change 'jp' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jp', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'jp' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jp_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jp_setup' );

function ju_called_files()
{

	wp_enqueue_style('wp-style', get_stylesheet_uri());
	wp_enqueue_style('site-icons', get_template_directory_uri() . '/flat-icon/flaticon.css');
	wp_enqueue_style('site-macbook', get_template_directory_uri() . '/assets/css/macbook.css');

	wp_enqueue_script('ju-sticky', get_template_directory_uri() . '/assets/js/sticky.compile.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script( 'ju-inViewport', get_template_directory_uri() . '/assets/js/isInViewport.js', array(), '20151215', true );
	wp_enqueue_script('ju-main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0.0', true);

}

add_action('wp_enqueue_scripts', 'ju_called_files');


function jp_post_types() {
  $labels = array(
    'name' => _x('Portfolio items', 'post type general name', 'SCRN'),
    'singular_name' => _x('Portfolio item', 'post type singular name', 'SCRN'),
    'add_new' => _x('Add', 'portfolio_item', 'SCRN'),
    'add_new_item' => __('Add a new portfolio item', 'SCRN'),
    'edit_item' => __('Edit portfolio item', 'SCRN'),
    'new_item' => __('New portfolio item', 'SCRN'),
    'all_items' => __('All portfolio items', 'SCRN'),
    'view_item' => __('View portfolio item details', 'SCRN'),
    'search_items' => __('Search portfolio item', 'SCRN'),
    'not_found' =>  __('No portfolio item found', 'SCRN'),
    'not_found_in_trash' => __('No portfolio item in the trash.' , 'SCRN'), 
    'parent_item_colon' => '',
    'menu_name' => 'Portfolio'

  );
  $args = array(
	'labels' => $labels,
	'menu_icon' => 'dashicons-clipboard',
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title', 'thumbnail')
  ); 
  
  register_post_type('portfolio',$args);
  register_taxonomy_for_object_type( 'category', 'portfolio' ); 
}
add_action('init', 'jp_post_types');



// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clients', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Client', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Clients', 'text_domain' ),
		'name_admin_bar'        => __( 'Clients', 'text_domain' ),
		'archives'              => __( 'Archives Clients', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Client', 'text_domain' ),
		'edit_item'             => __( 'Edit Client', 'text_domain' ),
		'update_item'           => __( 'Update Client', 'text_domain' ),
		'view_item'             => __( 'View Client', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Client', 'text_domain' ),
		'description'           => __( 'Client Description', 'text_domain' ),
		'labels'                => $labels,
		'menu_icon'          	=> 'dashicons-groups',
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'clients', $args );

}
add_action( 'init', 'custom_post_type', 0 );

/* Autoriser les fichiers SVG */
function wpc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');