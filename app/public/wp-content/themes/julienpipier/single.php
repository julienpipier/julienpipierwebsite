<?php

$projet_annee = get_field('projet_annee');
$projet_cms = get_field_object('projet_cms')['choices'][get_field('projet_cms')];
$projet_type = get_field_object('projet_type')['choices'][get_field('projet_type')];
get_header();
?>
	<div id="primary" class="content-area">
        <?php get_template_part( '/template-part/bloc', 'nav' ) ?>
        
        <section id="" class="flex-row full-height">
            <div class="col-md-6 offset-md-1 bg-green padding-50 singleorder1">
                    <div class="gallery_pix margin-top-10">
                    <?php 
                    $images = get_field('potfolio_gallery');

                    $size = 'large'; // (thumbnail, medium, large, full or custom size)
                    if( $images ): ?>
                        <?php foreach( $images as $image ):?>
                            <img src="<?php echo $image['sizes'][$size]; ?>" />
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5 offset-md-1 flex-center-start singleorder0">
                <div class="fixed-top offset-md-7">
                    <small class="go-back">
                        <a href="/projets">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                            </svg>
                            Retour aux projets
                        </a>
                    </small>
                    <h1><?php echo get_the_title(); ?></h1>
                    <div class="description-projet">
                        <?php echo get_field('portfolio_description_projet'); ?>
                        <?php the_content( ); ?>
                    </div>
                    <div class="description-type-projet">
                        <?php if($projet_type != 'none'): ?>
                            <div class="type_projet">TYPE / <span><?php echo $projet_type ?></span></div>
                        <?php endif; ?>
                        <?php if($projet_cms != 'none'): ?>
                            <div class="type_projet">CMS / <span><?php echo $projet_cms ?></span></div>
                        <?php endif; ?>
                        <?php if(!empty($projet_annee)): ?>
                            <div class="type_projet">ANNEE / <span><?php echo $projet_annee ?></span></div>
                        <?php endif; ?>
                    </div>
                    <?php if(!empty(get_field('link'))): ?>
                    <div class="flex-center-end">
                        <a href="<?php echo get_field('link'); ?>" target="_blank">Voir le site</a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>

    </div><!-- #primary -->
    
<?php
get_footer();
?>