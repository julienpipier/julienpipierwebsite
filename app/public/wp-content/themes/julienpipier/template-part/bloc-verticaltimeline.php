<div class="history-tl-container">
  <ul class="tl">
  <?php 
  $currents = end($current_task_projet) + 1;
    if( have_rows('timeline')):
    $i = 0;

    // Loop through rows.
    while ( have_rows('timeline') ) : the_row();
        $i++;
        $current  = ( $currents == $i) ? 'true' : 'false';
        $label    = get_sub_field('timeline_label');
        $picto    = get_sub_field('timeline_picto');
        $disabled = ($current != 'true' ) ? 'disabled' : '';
        $current = ($current == 'true' ) ? true : false;
    ?>
    <li class="tl-item <?php echo $disabled; ?>"  id="heading<?php echo $i; ?>" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="<?php echo $current ?>" aria-controls="collapse<?php echo $i; ?>">
        <div class="timestamp">
            <div class="item-title"><?php echo $label; ?></div>
        </div>
        
        <div class="item-detail"><img src="<?php echo $picto['url']; ?>" alt="<?php echo $picto['title']; ?>"/></div>
    </li>
    <?php 
    endwhile;
    endif;
    ?>
  </ul>
  <div>
                            <?php echo do_shortcode($retroplanning) ?>
                        </div>
</div>