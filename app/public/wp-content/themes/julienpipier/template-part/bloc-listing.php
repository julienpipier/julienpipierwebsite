
<section id="" class="flex-row">
  <div class="col-md-5 offset-2 offset-md-1">
      <div class="fixed">
        <h1><span>Mes</span><br/>réalisations</h1>
        <div class="col-md-5 col-xl-4 description-title row">
        <?php
        echo get_post_field('post_content', get_the_ID()); 
        ?>
        </div>

      </div>
  </div>
  <div class="col-md-6 offset-2 offset-md-0 bg-green">
  <?php if ( have_posts() ) : 
    $cat    =  array(
        '3'
    );
    $web    = ($_GET['type'] == 'web')? $cat[]='28':'';
    $design = ($_GET['type'] == 'design')? $cat[]='29':'';

    $args = array(
        'post_type'         => 'portfolio',
        'category__and'     => $cat,
        'posts_per_page'    => 20,
        'post_status'       => "publish",
        'has_password'      => false,
        'paged'             => get_query_var( 'paged' )
    );
    $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            $i=0;
            
            echo '<div class="blocItems flex-center-center hoverflowhidden ">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                    $i++;
                    set_query_var( 'loop', $i );
                    get_template_part( 'template-part/content', 'private' );
            }
        ?>
        <?php
            echo '</div>';
        } else {
            echo ' no posts found';
        }
    wp_reset_postdata();
    endif;
    ?>
  </div>
</section>
