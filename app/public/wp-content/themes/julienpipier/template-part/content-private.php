<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package DOH
 */
$loop = get_query_var('loop');
$order2 = ($loop%2)?'':'order2';

$cat = (get_field_object('projet_categorie')['choices'][get_field('projet_categorie')])? get_field_object('projet_categorie')['choices'][get_field('projet_categorie')]:'';

$cat = ($cat == 'Site web')? get_field_object('projet_type')['choices'][get_field('projet_type')] : $cat;

$visible = (get_field('loading'))? "load" : "";
$loading = (get_field('loading'))? "#" : get_the_permalink();
$refonte = (get_field('refonte'))? "#" : get_the_permalink();

?>

<div class="item <?php echo $order2.' '.$visible?>">
    <div class="category">
        <div class="rotate90">
            <a href=""><?php echo $cat ?></a>
        </div>
    </div>
    <div class="content">
        <a href="<?php echo $loading ?>">
            <div class="pix">
                <?php echo the_post_thumbnail('small'); ?>
            </div>
            <div class="project-title">
                <div class="cat"><?php echo get_field('projet_profession') ?></div>
                <h4><?php the_title(); ?></h4>
            </div>
        </a>
    </div>
    <?php if($loading == "#" && $refonte != "#"): ?>
        <div class="loading">
            <h4>En cours de production</h4>
        </div>
    <?php endif; ?>
    <?php if($refonte == "#"): ?>
        <div class="loading">
            <h4>Refonte en cours</h4>
        </div>
    <?php endif; ?>
</div>

