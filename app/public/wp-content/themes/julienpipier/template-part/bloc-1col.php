<section id="section" class="flex-row flex-center-center bordermenu">
  <div class="col-md-10 offset-md-1 flex-center-center wrap">
  <div class="timeline-wrap" id="accordion">
    <ul class="timeline">
    <?php 
    if( have_rows('timeline')):
    $i = 0;

    // Loop through rows.
    while ( have_rows('timeline') ) : the_row();
        
        $i++;
        $label = get_sub_field('timeline_label');
        $picto = get_sub_field('timeline_picto');
        $desc  = get_sub_field('timeline_desc');

        $show = ($i == 1)?"show":"";

        $labelArray[] = sanitize_title_with_dashes($label);

    ?>
      <!-- <input class="toggle-box" id="identifier-<?php echo $i ?>" type="checkbox"> -->
      <li class="timeline-item bmw showSingle" id="heading<?php echo $i ?>" target="<?php echo $i ?>">
        <a href="#<?php echo sanitize_title_with_dashes( $label ); ?>" class="p-timeline-item">
          <label for="identifier-<?php echo $i ?>"><div class="p-timeline-block"><img src="<?php echo $picto['sizes']['thumbnail']; ?>" alt="<?php echo $picto['title']; ?>"></div></label>
          <span class="p-timeline-carmodel" data-car="<?php echo $i; ?>"><?php echo $label; ?></span>
        </a>
      </li>
      
    <?php 
    endwhile;
    ?>
    </ul>
    <section class="cnt">
    <?php 
    $j=0;
    $numLabel = 0;

  

    while ( have_rows('timeline') ) : the_row();    
    $j++;
    $desc  = get_sub_field('timeline_desc');
    ?>
      <div id="<?php echo $labelArray[$numLabel] ?>">

        <div id="div<?php echo $j ?>" class="targetDiv">
          <?php echo $desc ?>
          <?php if($j != 1): ?>
            <div class="carousel-control-prev timeline-item bmw" role="button" data-slide="prev" id="heading<?php echo $j-1 ?>" target="<?php echo $j-1 ?>">
              <span class="carousel-control-prev-icon" aria-hidden="true" data-car="<?php echo $j-1; ?>"></span>
              <span class="sr-only">Previous</span>
            </div>
          <?php endif; ?>
          <?php if($j < 6): ?>
            <div class="carousel-control-next timeline-item bmw" role="button" data-slide="next" id="heading<?php echo $j+1 ?>" target="<?php echo $j+1 ?>">
              <span class="carousel-control-next-icon" aria-hidden="true" data-car="<?php echo $j+1; ?>"></span>
              <span class="sr-only">Next</span>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php 
    $numLabel++;
    endwhile;
    ?>
    </section>
    <?php 
    endif;
      ?> 
  </div>
</div>
</section>
