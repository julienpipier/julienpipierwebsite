<?php

if( get_row_layout() != 'services' ): 

$image          = get_sub_field('2col_image');
$content        = get_sub_field('2col_content', false, false);
$title          = get_sub_field('2col_title');

?>

<section id="section2col" class="flex-row flex-center-center bordermenu">
  <div class="col-md-5 offset-md-1 flex-center-center">
      <h2><?php echo $title ?></h2>
      <div class="content-about">
          <p class="p-justifiy">
              <?php echo $content; ?>
          </p>
      </div>
  </div>
  <div class="col-md-6 flex-center-center">
    <img src="<?php echo $image['url']; ?>"/>
  </div>

</section>

<?php 

else: 
$picto          = get_sub_field('service_picto');
$baseline       = get_sub_field('service_baseline', false, false);
$title          = get_sub_field('service_titre');
?>

<section id="section-services" class="flex-row full-height bordermenu">
  <div class="col-md-5 offset-md-1">
      <div class="d-none d-sm-block bandeau black full-height" data-sticky-container>
          <div class="flex-center-end">
            <h2 class="moving-title" data-sticky data-sticky-wrap data-margin-bottom="180" data-margin-top="100"><?php echo $title ?></h2>
            <div class="flex-center-end">
              <p class="white"><?php echo $baseline; ?></p>
              <div class="content-coffee">
                <img src="<?php echo $picto['sizes']['thumbnail']; ?>"/>
              </div>
            </div>
          </div>
      </div>
      <div class="d-block d-sm-none d-md-none bandeau black full-height">
          <div class="flex-center-end">
            <h2 class="moving-title"><?php echo $title ?></h2>
            <div class="flex-center-end">
              <p class="white"><?php echo $baseline; ?></p>
              <div class="content-coffee">
                <img src="<?php echo $picto['sizes']['thumbnail']; ?>"/>
              </div>
            </div>
          </div>
      </div>
  </div>
  <div class="col-md-6 flex-center-center">
    <?php 
 
      if( have_rows('service_presta')):


        // Loop through rows.
        while ( have_rows('service_presta') ) : the_row();

          $sub_title      = get_sub_field('service_presta_sub_title');
          $content        = get_sub_field('service_presta_content', false, false);
          $title          = get_sub_field('service_presta_title');
          $link           = (empty(get_sub_field('service_presta_link')) ? '#' : get_sub_field('service_presta_link') );
        ?>
            <a href="<?php echo $link; ?>" class="repeater-content">
              <h4><?php echo $title; ?></h4>
              <h5><?php echo $sub_title; ?></h5>
              <div class="info">          
                <p class="p-justifiy">
                    <?php echo $content; ?>
                </p>
              </div>
            </a>
        <?php
        endwhile;
      endif;
    ?>
  </div>
  <div class="footer-black">
    <div class="d-block d-sm-none">
      <p>On commence par un café ? </p>
    </div>
  </div>
</section>


<?php 
endif;