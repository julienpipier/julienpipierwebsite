
<section id="section-realisation" class="flex-row flex-center-center bordermenu">
  <div class="col-md-5 offset-md-1 flex-center-center paddingLeft120">
      <div class="content">
          <p class="p-justifiy">
            <?php echo get_sub_field('realisation_description') ?>
          </p>
      </div>
  </div>
  <div class="col-md-6 flex-center-center">
    <h2>
        <?php echo get_sub_field('realisation_title') ?>
    </h2>
  </div>
  <div class="col-md-6">
    <div class="mockup_wrap">
        <div class="md-imac md-glare">
            <div class="md-body">
                <div class="md-top">
                    <div class="md-camera"></div>
                    <div class="md-screen">
                        <a href="<?php echo get_sub_field('imac_screen_link')['url'] ?>">
                            <img src="<?php echo get_sub_field('imac_screen')['url'] ?>">
                        </a>
                    </div>
                </div>
            </div>
            <div class="md-base">
                <div class="md-stand"></div>
                <div class="md-foot"></div>
            </div>
            <a href="<?php echo get_sub_field('imac_screen_link')['url'] ?>" class="description-imac title-screen">
                <?php echo get_sub_field('imac_screen_link')['title'] ?>
            </a>
        </div>

        <div class="md-macbook-pro md-glare">
            <div class="md-lid">
                <div class="md-camera"></div>
                <div class="md-screen">
                    <a href="<?php echo get_sub_field('macbook_screen_link')['url'] ?>">
                        <img src="<?php echo get_sub_field('macbook_screen')['url'] ?>">
                    </a>
                </div>
            </div>
            <div class="md-base"></div>
            <a href="<?php echo get_sub_field('macbook_screen_link')['url'] ?>" class="description-macbook title-screen">
                <?php echo get_sub_field('macbook_screen_link')['title'] ?>
            </a>
        </div>

        <div class="md-ipad md-black-device md-glare">
            <div class="md-body">
                <div class="md-front-camera"></div>
                <div class="md-screen">
                    <a href="<?php echo get_sub_field('ipad_screen_link')['url'] ?>">
                        <img src="<?php echo get_sub_field('ipad_screen')['url'] ?>">
                    </a>
                </div>
                <button class="md-home-button"></button>
            </div>  
            <a href="<?php echo get_sub_field('ipad_screen_link')['url'] ?>" class="description-ipad title-screen">
                <?php echo get_sub_field('ipad_screen_link')['title'] ?>
            </a>
        </div>

        <div class="md-iphone-5 md-black-device md-glare left-mockup">
            <div class="md-body">
                <div class="md-buttons"></div>
                <div class="md-front-camera"></div>
                <div class="md-top-speaker"></div>
                <div class="md-screen">
                    <a href="<?php echo get_sub_field('iphone_screen_link')['url'] ?>">
                        <img src="<?php echo get_sub_field('iphone_screen')['url'] ?>">
                    </a>
                </div>
                <button class="md-home-button"></button>
            </div>
            <a href="<?php echo get_sub_field('iphone_screen_link')['url'] ?>" class="description-iphone title-screen">
                <?php echo get_sub_field('iphone_screen_link')['title'] ?>
            </a>
        </div>
    </div>
  </div>
</section>
<section id="section-more" class="flex-end-end bordermenu">
    <div class="col-md-12">
        <a class="aftertrait" href="/projets">En voir plus</a>
    </div>
</section>
