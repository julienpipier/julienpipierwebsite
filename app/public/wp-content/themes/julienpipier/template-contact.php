<?php
/*
Template Name: Contact
*/


get_header();

?>

	<div id="primary" class="content-area">
    <?php get_template_part( '/template-part/bloc', 'nav' ); ?>


    <section id="contact" class="flex-row full-height">
        <div class="col-md-5 offset-md-1">
            <h1>Contact</h1>
            <div class="content">
                <?php the_content(); ?>
                <div class="offset-md-1 col-md-4 flex-center-center form-content d-block d-sm-none">
                    <?php echo do_shortcode( '[contact-form-7 id="252" title="Contact"]' ) ?>
                </div>
                <div class="instagram">
                    INSTAGRAM
                    <?php echo do_shortcode( '[instagram-feed num=6 cols=3 showbutton=false showheader=false showfollow=false height=100 heightunit=%]' ) ?>
                </div>
            </div>
        </div>
        <div class="offset-md-1 col-md-4 flex-center-center form-content d-none d-sm-block">
            <?php echo do_shortcode( '[contact-form-7 id="252" title="Contact"]' ) ?>
        </div>
    </section>
    
	</div><!-- #primary -->

<?php
get_footer();
