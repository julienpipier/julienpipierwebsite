<?php


get_header();

?>

	<div id="primary" class="content-area">
    <?php get_template_part( '/template-part/bloc', 'nav' ) ?>
    <?php if(get_the_ID() != 472): ?>
        <main role="main" class="container-fluid no-padding">
            <div class="col-lg-6" style="background: url(<?php echo get_field('back_img') ?>) center center; background-size:cover;"></div>
            <div class="col-lg-6 flex-center-center">
                <p class="lead">Julien Pipier</p>
                <h1>Développeur web</h1>
                <div class="bottom">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chevron-compact-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1.553 6.776a.5.5 0 0 1 .67-.223L8 9.44l5.776-2.888a.5.5 0 1 1 .448.894l-6 3a.5.5 0 0 1-.448 0l-6-3a.5.5 0 0 1-.223-.67z"/>
                    </svg>
                </div>
            </div>
        </main><!-- /.container -->
    <?php endif; ?>
    <?php 
    if( have_rows('blocs') ):

        // Loop through rows.
        while ( have_rows('blocs') ) : the_row();
            // Case: 2 colonness.
            if( get_row_layout() == '2_colonnes' ): 
                get_template_part( 'template-part/bloc', '2col' );
            elseif( get_row_layout() == 'services' ): 
                get_template_part( 'template-part/bloc', '2col' );
            elseif( get_row_layout() == 'timeline' ): 
                get_template_part( 'template-part/bloc', '1col' );
            elseif( get_row_layout() == 'realisations' ): 
                get_template_part( 'template-part/bloc', 'realisation' );
            elseif( get_row_layout() == 'liste_articles' ):
                get_template_part( 'template-part/bloc', 'listing' );
                
            endif;
        endwhile;
    endif;
    ?>
     <?php get_template_part( '/template-part/bloc', 'footer' ) ?>
	</div><!-- #primary -->
<?php
get_footer();
