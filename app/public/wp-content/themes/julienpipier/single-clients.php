<?php
get_header();
?>
	<div id="primary" class="content-area">
        <?php get_template_part( '/template-part/bloc', 'nav' ) ?>
        
        <section class="flex-row full-height">
            <div class="col-md-7 offset-md-1 padding-50">
                <h1 class="padding-50"><?php echo get_the_title(); ?></h1>
                <div class="accordion" id="accordionExample">
                    <div class="carde">
                    <?php

                    // Check rows exists.
                    if( have_rows('projet') ):
                        $loop = 0;
                        // Loop through rows.
                        while( have_rows('projet') ) : the_row();

                            // Load sub field value.
                            $loop++;
                            $link_pinterest         = get_sub_field('link_pinterest');
                            $date_pinterest         = get_sub_field('date_pinterest');
                            $date_pinterest_fin     = get_sub_field('date_pinterest_fin');
                            $contenus               = get_sub_field('contenus');
                            $date_content           = get_sub_field('date_content');
                            $date_content_fin       = get_sub_field('date_content_fin');
                            $devis_projet           = get_sub_field('devis_projet');
                            $wireframe              = get_sub_field('wireframe');
                            $date_wireframe         = get_sub_field('date_wireframe');
                            $date_wireframe_fin     = get_sub_field('date_wireframe_fin');
                            $photoshop              = get_sub_field('photoshop');
                            $date_photoshop         = get_sub_field('date_photoshop');
                            $date_photoshop_fin     = get_sub_field('date_photoshop_fin');
                            $link_preprod_projet    = get_sub_field('link_preprod_projet');
                            $link_jira_projet       = get_sub_field('link_jira_projet');
                            $current_task_projet    = get_sub_field('current_task_projet');
                            $nom_projet             = get_sub_field('nom_projet');
                            $cafe                   = get_sub_field('content_cafe');
                            $content_dev            = get_sub_field('content_dev');
                            $retroplanning          = get_sub_field('retroplanning_projet_copy');

                            $show = (end($current_task_projet) == 0) ? 'show' : '';
                            $show1 = (end($current_task_projet) == 1) ? 'show' : '';
                            $show2 = (end($current_task_projet) == 2) ? 'show' : '';
                            $show3 = (end($current_task_projet) == 3) ? 'show' : '';

                    ?>


                        <div id="collapse1" class="collapse <?php echo $show; ?>" aria-labelledby="heading1" data-parent="#accordionExample">
                            <div class="cardbody">
                                <?php echo $cafe; ?>                            
                            </div>
                        </div>
                        <div id="collapse2" class="collapse <?php echo $show1; ?>" aria-labelledby="heading2" data-parent="#accordionExample">
                            <div class="cardbody">
                                <a data-pin-do="embedBoard" data-pin-lang="fr" data-pin-board-width="900" data-pin-scale-height="440" data-pin-scale-width="160" href="<?php echo $link_pinterest; ?>"></a>
                            </div>
                        </div>
                        <div id="collapse3" class="collapse <?php echo $show2; ?>" aria-labelledby="heading3" data-parent="#accordionExample">
                            <div class="cardbody">
                                <ul>
                                    <li>Wireframe</li>
                                    <li>Photoshop</li>
                                </ul>   
                            </div>
                        </div>

                        <div id="collapse4" class="collapse <?php echo $show3; ?>" aria-labelledby="heading4" data-parent="#accordionExample">
                            <div class="cardbody">
                                <?php echo $content_dev ?>
                            </div>
                        </div>
                        <div>
                            <?php echo do_shortcode($retroplanning) ?>
                        </div>
                    <?php

                        // End loop.
                        endwhile;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1 padding-50">


                <?php 
                if( have_rows('blocs', 243) ):
                    set_query_var( 'retroplanning', $retroplanning );
                    set_query_var( 'current_task_projet', $current_task_projet );

                    // Loop through rows.
                    while ( have_rows('blocs', 243) ) : the_row();
                        if( get_row_layout() == 'timeline' ): 
                            get_template_part( 'template-part/bloc', 'verticaltimeline' );
                        endif;
                    endwhile;
                endif;
                ?>

            </div>
        </section>

    </div><!-- #primary -->

<?php
get_footer();
?>