<?php

get_header();

?>

	<div id="primary" class="content-area">
    <?php get_template_part( '/template-part/bloc', 'nav' );
    if( have_rows('blocs') ):

        // Loop through rows.
        while ( have_rows('blocs') ) : the_row();
            // Case: 2 colonness.
            if( get_row_layout() == '2_colonnes' ): 
                get_template_part( 'template-part/bloc', '2col' );
            elseif( get_row_layout() == 'services' ): 
                get_template_part( 'template-part/bloc', '2col' );
            elseif( get_row_layout() == 'timeline' ): 
                get_template_part( 'template-part/bloc', '1col' );
            elseif( get_row_layout() == 'realisations' ): 
                get_template_part( 'template-part/bloc', 'realisation' );
            elseif( get_row_layout() == 'liste_articles' ):
                get_template_part( 'template-part/bloc', 'listing' );
                
            endif;
        endwhile;
    endif;
    ?>
    
	</div><!-- #primary -->

<?php
get_footer();