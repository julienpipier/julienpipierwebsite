<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DOH
 */

?>
	</div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  	<div class="menufixed">
			<nav class="navbar navbar-expand-md navbar-light" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'depth'           => 1, // 1 = no dropdowns, 2 = with dropdowns.
						'container'       => 'div',
						'container_class' => 'navbar-collapse',
						'container_id'    => 'nav',
						'menu_class'      => 'navbar-nav mr-auto flex-center-center flex-col',
					) );
				?>
			</nav>			
		</div>
      </div>
    </div>
  </div>
</div>
<?php wp_footer(); ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<?php 
	if($post->post_type == 'clients'){
		echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
	} 
?>
</body>
</html>
